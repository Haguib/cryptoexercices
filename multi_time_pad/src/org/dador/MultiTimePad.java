
package org.dador;


/**Haguibou TALL 11112255 & Abdoulaye DIALLO 11214263
 *
 */

public class MultiTimePad {


    /**
     * Main function. Loads cryptogram and displays decryption
     * @param args
     */
    public static void main(final String[] args) {
        String msg0 = "BB3A65F6F0034FA957F6A767699CE7FABA855AFB4F2B520AEAD612944A801E";
        String msg1 = "BA7F24F2A35357A05CB8A16762C5A6AAAC924AE6447F0608A3D11388569A1E";
        String msg2 = "A67261BBB30651BA5CF6BA297ED0E7B4E9894AA95E300247F0C0028F409A1E";
        String msg3 = "A57261F5F0004BA74CF4AA2979D9A6B7AC854DA95E305203EC8515954C9D0F";
        String msg4 = "BB3A70F3B91D48E84DF0AB702ECFEEB5BC8C5DA94C301E0BECD241954C831E";
        String msg5 = "A6726DE8F01A50E849EDBC6C7C9CF2B2A88E19FD423E0647ECCB04DD4C9D1E";
        String msg6 = "BC7570BBBF1D46E85AF9AA6C7A9CEFA9E9825CFD5E3A0047F7CD009305A71E";
        String[] messages = new String[]{msg0, msg1, msg2, msg3, msg4, msg5, msg6};
        byte[] key;
        byte[][] byteArrayMsg;
        int nbMsg = messages.length;
        byte[] tmpByteMsg;
        int i;
        byteArrayMsg = new byte[nbMsg][];

        System.out.println("Original Cryptograms :");
        for (i = 0; i < nbMsg; i++) {
            System.out.println(messages[i]);
            tmpByteMsg = HexConverters.toByteArrayFromHex(messages[i]);
            byteArrayMsg[i] = tmpByteMsg ;
        }

        System.out.println();
        System.out.println("result of XOR");
        System.out.println();
        System.out.println(HexConverters.toHexFromByteArray(HexConverters.xorArray(HexConverters.toByteArrayFromHex(messages[0]), HexConverters.toByteArrayFromHex(messages[1]))));
        System.out.println(HexConverters.toHexFromByteArray(HexConverters.xorArray(HexConverters.toByteArrayFromHex(messages[0]), HexConverters.toByteArrayFromHex(messages[2]))));
        System.out.println(HexConverters.toHexFromByteArray(HexConverters.xorArray(HexConverters.toByteArrayFromHex(messages[0]), HexConverters.toByteArrayFromHex(messages[3]))));
        System.out.println(HexConverters.toHexFromByteArray(HexConverters.xorArray(HexConverters.toByteArrayFromHex(messages[0]), HexConverters.toByteArrayFromHex(messages[4]))));
        System.out.println(HexConverters.toHexFromByteArray(HexConverters.xorArray(HexConverters.toByteArrayFromHex(messages[0]), HexConverters.toByteArrayFromHex(messages[5]))));
        System.out.println(HexConverters.toHexFromByteArray(HexConverters.xorArray(HexConverters.toByteArrayFromHex(messages[0]), HexConverters.toByteArrayFromHex(messages[6]))));
        System.out.println();
        
       
         
        key = new byte[msg1.length() / 2];
        // Fill in the key ...


        key[0] = (byte)0x49^(byte)0xBB;
        key[1]= (byte)0x1a;
        key[2] = 0x04;
        key[3] = (byte)0x9B;
        key[4] = (byte)0xD0;
        key[5] = 0x73;
        key[6] = (byte)0x48^(byte)0x6B;
        key[7] = (byte)0xC8;
        key[8] = (byte)0x6E^(byte)0x57;
        key[9] = (byte)0x6E^(byte)0xF6;
        key[10] = (byte)0x69^(byte)0xA7;
        key[11] = (byte)0x6E^(byte)0x67;
        key[12] = 0x0E;
        key[13] = (byte)0x20^(byte)0x9C;
        key[14] = (byte)0x86;
        key[15] = (byte)0xda;
        key[16] = (byte)0x73^(byte)0xBA;
        key[17] = (byte)0x62^(byte)0x82;
        key[18] = (byte)0x63^(byte)0x5A;
        key[19] = (byte)0x89;
        key[20] = (byte)0x74^(byte)0x5E;
        key[21] = 0x5F;
        key[22] = 0x72;
        key[23] = 0x67;
        key[24] =(byte) 0x83;
        key[25] = (byte)0xA5;
        key[26] = 0x61;
        key[27] = (byte)0xFD;
        key[28] = 0x25;
        key[29] = (byte)0x6E^(byte)0x80;
        key[30] = (byte)0x2E^(byte)0x1E;
        
        System.out.println("Key :");
        System.out.println(HexConverters.toHexFromByteArray(key));

        System.out.println();
        System.out.println("Decoded messages :");
        for (i = 0; i < nbMsg; i++) {
            tmpByteMsg = HexConverters.xorArray(key, byteArrayMsg[i]);
            System.out.println(HexConverters.toPrintableString(tmpByteMsg));
        }
    }
}
